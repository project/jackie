<?php

class JackieBuildBreadcrumbAction extends RulesActionHandlerBase {

  /**
   * Defines the action.
   */
  public static function getInfo() {
    return array(
      'name' => 'jackie_build_breadcrumb',
      'label' => t('Build breadcrumb navigation'),
      'group' => 'Jackie',
      'parameter' => array(

      ),
    );
  }

  /**
   * Executes the action.
   */
  public function execute() {
    drupal_set_message('test');
    $breadcrumb = array();
    $breadcrumb[] = l('test', 'test');
    drupal_set_breadcrumb($breadcrumb);
  }
}

