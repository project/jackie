<?php

/**
 * @file
 * Contains class JackiePathCondition.
 */

/**
 * Provides condition for Rules to match a page path.
 */
class JackiePathCondition extends RulesConditionHandlerBase {

  /**
   * Provides information for Rules about Jackie condition.
   */
  public static function getInfo() {

    $info =  array(
      'name' => 'jackie_path_condition',
      'label' => t('Match page path'),
      'help' => t('Checks if path of a load page matches entered path template.'),
      'parameter' => array(
        'path' => array(
          'type' => 'text',
          'label' => t('For which path(s) event should be triggered'),
          'description' => t('You may create named placeholders for variable parts of the path by using %name (for required) and !name (for optional) elements. These named placeholders can be turned into contexts using Jackie actions.'),
          'restriction' => 'input',
        ),
      ),
      'group' => 'Jackie',
    );

    return $info;
  }

  /**
   * Alters form with default Rules condition elements.
   */
  public function form_alter(&$form, $form_state, $options) {

    $examples = array(
      t('%all - Trigger event for every page.', array('%all' => '<all>')),
      t('%front - Trigger event for the front page.', array('%front' => '<front>')),
      t('%custom - Trigger event for "my_page" url (or its alias).', array('%custom' => 'my_page')),
      t('%node - Trigger event for "node/[nid]" (or its alias) page.', array('%node' => 'node/%node')),
      t('%user - Trigger event for "user/[uid]/edit" (or its alias) page.', array('%user' => 'user/%user/edit')),
      t('%taxonomy - Trigger event for "taxonomy/term/[tid]" (or its alias) page.', array('%taxonomy' => 'taxonomy/term/%term')),
      t('%custom - Trigger event for "custom/[nid]/[uid]/page" (or its alias) page.', array('%custom' => 'custom/%node/%user/page')),
      t('%custom - Trigger event for "custom/[nid]" and "custom/[nid]/[uid]" (or their aliases) pages.', array('%custom' => 'custom/%node/!user')),
      t('%custom - Trigger event for "foo/[cid]/comment?key=[nid]" (or its alias) page.', array('%custom' => 'foo/%comment/comment?key=%node')),
    );

    // Create an examples for end users.
    $form['parameter']['path']['examples'] = array(
      '#type' => 'fieldset',
      '#title' => t('Examples'),
      '#description' => theme('item_list', array('items' => $examples)),
    );

    // Change form type and title for "Path" element for a better usability.
    $form['parameter']['path']['settings']['path']['#type'] = 'textfield';
    $form['parameter']['path']['settings']['path']['#title'] = t('Path');

    // Hide help form item, because we don't need it for a path element.
    $form['parameter']['path']['settings']['help']['#access'] = FALSE;
  }

  /**
   * Valides path that was entered by a user.
   */
  public function validate() {

    // Correct some mistakes that user might make.
    $path = explode('/', $this->element->settings['path']);

    $correct_path = array();
    foreach ($path as $bit) {
      if (!$trimmed_bit = trim($bit)) {
        continue;
      }

      $correct_path[] = $trimmed_bit;
    }

    // Ensure that all optional arguments are located in the end of path.
    $optional_arg_exist = FALSE;
    foreach ($correct_path as $bit) {

      // If optional argument already exists, but next argument is not optional.
      if ($optional_arg_exist && $bit[0] != '!') {
        throw new RulesIntegrityException(t('Optional arguments may be located only at the end of path.'), array(NULL, 'path'));
      }
      // Set proper flag if we found an optional argument in path.
      elseif (!$optional_arg_exist && $bit[0] == '!') {
        $optional_arg_exist = TRUE;
      }
    }

    // @TODO: handle $_get query with arguments.

    // Ensure that all arguments has names and they are unique.
    $arguments = array();
    foreach ($correct_path as $bit) {
      if (in_array($bit[0], array('!', '%'))) {
        if (drupal_strlen($bit) === 1) {
          throw new RulesIntegrityException(t('Invalid arg %argument. All arguments must be named with keywords.', array('%argument' => $bit[0])), array(NULL, 'path'));
        }
        elseif(in_array($bit, $arguments)) {
          throw new RulesIntegrityException(t('Duplicate argument name %argument. All names should be unique.', array('%argument' => $bit)), array(NULL, 'path'));
        }
        else {
          $arguments[] = $bit;
        }
      }
    }

    // Save a correct path.
    $this->element->settings['path'] = implode('/', $correct_path);
  }

  /**
   * Callback that checks if current path matches pre-defined path template.
   */
  public function execute() {
    $path_template = $this->element->settings['path'];
    $current_path = current_path();

    // @TODO: Validate path.
    return TRUE;
  }

}
