<?php

/**
 * @file
 * Contains class JackiePageBuildEvent.
 */

/**
 * Describes class that contains Jackie event for building a page data.
 */
class JackiePageBuildEvent extends RulesEventDefaultHandler {

  /**
   * Describe main Jackie event for Rules.
   */
  public static function getInfo() {
    return array(
      'name' => 'jackie_page_build',
      'label' => t('Page is building'),
      'group' => 'Jackie',
    );
  }

}
